package com.gerardas.platDesktop.Entities;

import com.badlogic.gdx.graphics.Color;
import com.badlogic.gdx.graphics.g2d.SpriteBatch;
import com.badlogic.gdx.graphics.g2d.TextureRegion;
import com.badlogic.gdx.scenes.scene2d.Actor;
import com.gerardas.platDesktop.GameStage;
import com.gerardas.platDesktop.Level;
import com.gerardas.platDesktop.Objects;
import com.gerardas.platDesktop.Textures;
import com.gerardas.platDesktop.Utils;

public class Turreta extends Actor{

	GameStage stage;

	public int frame,Dframe;
	public float frameDt;
	public float bulletDt;
	public boolean golden;

	private TextureRegion texr;


	public Turreta(float x, float y, GameStage s){

		this.setPosition(x, y);
		this.setSize(64, 64);
		this.setOrigin(32, 32);
		this.stage=s;

		texr=new TextureRegion(Objects.tex);
		texr.setRegion(
				64*8,
				64*2,
				64,
				64);
		
		
		frame=0;

	}





	@Override
	public void act(float dt){
		super.act(dt);

		frameDt+=dt;

		while(frameDt>=0.09f){
			frameDt-=0.09f;
			if(frame>Dframe){
				frame--;
			}else if (frame<Dframe){
				frame++;
			}
			
		}
		
		
		texr.setRegion(
				64*(8+frame),
				64*2,
				64,
				64);
		
		searchAndDestroy(dt);
			
	}




	private void searchAndDestroy(float dt) {
		float px=((GameStage)getStage()).getPlayer().getCenterX();
		float py=((GameStage)getStage()).getPlayer().getCenterY();
		if(Utils.dist(getCenterX(), getCenterY(), px,py)<350){
			float r=(float) Math.atan((py-getCenterY())/(px-getCenterX()))/2/3.14f*360+90;
			
			if(px<getCenterX()){
				r+=180;
			}
			setRotation(r);
			Dframe=0;
			
			
			bulletDt+=dt;
			if(bulletDt>=1){
				bulletDt-=1;
				search:
				for (Actor a : stage.bullets.getChildren()) {
					
					if(!((Bullet)a).isActive()){
						((Bullet)a).fire(this.getCenterX(), this.getCenterY(), this.getRotation());	
						break search;
					}
				}
			}

			
			
		}else{
			Dframe=3;
		}
	}





	@Override
	public void draw(SpriteBatch batch, float arg1) {

	




		if(Level.DrawShadows){
			batch.setColor(0,0,0,Level.IntShadows);

			batch.draw(texr,
					getX()-(float) (-((Level.dxS/1.5)*2)*Math.sin(((stage.cameraAngle+225)/360)*2*Math.PI)),
					getY()-(float) (-((Level.dxS/1.5)*2)*Math.cos(((stage.cameraAngle+225)/360)*2*Math.PI)),
					32,
					32,
					getWidth(),
					getHeight(),
					getScaleX(),
					getScaleY(),
					getRotation());

			batch.setColor(Color.WHITE);
		}

		batch.draw(texr,
				getX(),
				getY(),
				32,
				32,
				getWidth(),
				getHeight(),
				getScaleX(),
				getScaleY(),
				getRotation());

		
		batch.draw(Textures.punt,
				getCenterX(),
				getCenterY());

	}




	public float getCenterX(){
		return getX()+getWidth()/2;
	}
	
	public float getCenterY(){
		return getY()+getHeight()/2;
	}




}
