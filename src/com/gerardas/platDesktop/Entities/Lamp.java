package com.gerardas.platDesktop.Entities;

import javax.swing.Box;

import box2dLight.ConeLight;
import box2dLight.PointLight;

import com.badlogic.gdx.graphics.Color;
import com.badlogic.gdx.graphics.g2d.SpriteBatch;
import com.badlogic.gdx.graphics.g2d.TextureRegion;
import com.badlogic.gdx.physics.box2d.Body;
import com.badlogic.gdx.scenes.scene2d.Actor;
import com.gerardas.platDesktop.BoxWorld;
import com.gerardas.platDesktop.GameStage;
import com.gerardas.platDesktop.Level;
import com.gerardas.platDesktop.Objects;
import com.gerardas.platDesktop.Textures;

public class Lamp extends Actor{

	GameStage stage;
	BoxWorld bw;
	private Body[] bodys;
	PointLight pl;

	private TextureRegion texl,texc;



	public Lamp(float x, float y,int i,int j, GameStage s){

		this.setPosition(x, y);
		this.setSize(64, 64);
		this.setOrigin(getWidth()/2, getHeight()/2);
		this.stage=s;
		this.texl = new TextureRegion(Objects.tex, 64*2, 64*7, 64, 64);
		this.texc = new TextureRegion(Objects.tex, 64*3, 64*7, 64, 64);



		bw=stage.screen.boxworld;

		bodys = bw.createChain(bw.bodyMap[i-1][j]);
		
		pl = new PointLight(bw.rayHandler, 100, new Color(1,1,1,.2f), bw.scale*7,  bodys[bodys.length-1].getPosition().x,bodys[bodys.length-1].getPosition().y);

	}





	@Override
	public void act(float dt){
		super.act(dt);
		pl.setPosition(bodys[bodys.length-1].getPosition());
		setPosition(bodys[bodys.length-1].getPosition().x/BoxWorld.scale*64-32,bodys[bodys.length-1].getPosition().y/BoxWorld.scale*64-32);
		setRotation(bodys[bodys.length-1].getAngle()/2/3.14f*360);
		//TODO potser no es necessari tota lestona

	}
	
	
	private float box2stage(float p){
		return p/BoxWorld.scale*64-32;
	}







	@Override
	public void draw(SpriteBatch batch, float arg1) {
		if(Level.DrawShadows){
			batch.setColor(0,0,0,Level.IntShadows);
			
			for (int i = 0; i < bodys.length-1; i++) {
				batch.draw(texc,
						box2stage(bodys[i].getPosition().x)-(float) (-((Level.dxS/1.5)*2)*Math.sin(((stage.cameraAngle+225)/360)*2*Math.PI)),
						box2stage(bodys[i].getPosition().y)-(float) (-((Level.dxS/1.5)*2)*Math.cos(((stage.cameraAngle+225)/360)*2*Math.PI)),
						getOriginX(),
						getOriginY(),
						getWidth(),
						getHeight(),
						getScaleX(),
						getScaleY(),
						bodys[i].getAngle()/2/3.14f*360);
				
			}
			
			batch.draw(texl,
					getX()-(float) (-((Level.dxS/1.5)*2)*Math.sin(((stage.cameraAngle+225)/360)*2*Math.PI)),
					getY()-(float) (-((Level.dxS/1.5)*2)*Math.cos(((stage.cameraAngle+225)/360)*2*Math.PI)),
					getOriginX(),
					getOriginY(),
					getWidth(),
					getHeight(),
					getScaleX(),
					getScaleY(),
					getRotation());

			
			batch.setColor(Color.WHITE);
		}
		
		
		for (int i = 0; i < bodys.length-1; i++) {
			batch.draw(texc,
					box2stage(bodys[i].getPosition().x),
					box2stage(bodys[i].getPosition().y),
					getOriginX(),
					getOriginY(),
					getWidth(),
					getHeight(),
					getScaleX(),
					getScaleY(),
					bodys[i].getAngle()/2/3.14f*360);
			
		}
		
		
		batch.draw(texl, getX(), getY(), getOriginX(), getOriginY(), getWidth(), getHeight(), getScaleX(), getScaleY(),
				getRotation());		
		
		
		batch.draw(Textures.punt,
				getCenterX(),
				getCenterY());

	}
	
	




	public float getCenterX(){
		return getX()+getWidth()/2;
	}
	
	public float getCenterY(){
		return getY()+getHeight()/2;
	}




}
