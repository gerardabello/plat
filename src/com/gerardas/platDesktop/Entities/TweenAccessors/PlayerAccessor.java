package com.gerardas.platDesktop.Entities.TweenAccessors;

import aurelienribon.tweenengine.TweenAccessor;

import com.gerardas.platDesktop.Entities.Player;

//We implement TweenAccessor<Particle>. Note the use of a generic.

public class PlayerAccessor implements TweenAccessor<Player> {

	// The following lines define the different possible tween types.
	// It's up to you to define what you need :-)

	public static final int POSITION_X = 1;
	public static final int POSITION_Y = 2;
	public static final int POSITION_XY = 3;
	public static final int ROTATION = 4;
	public static final int SCALE = 5;

	// TweenAccessor implementation

	@Override
	public int getValues(Player target, int tweenType, float[] returnValues) {
		switch (tweenType) {
		case POSITION_X: returnValues[0] = target.getCenterX(); return 1;
		case POSITION_Y: returnValues[0] = target.getCenterY(); return 1;
		case POSITION_XY:
			returnValues[0] = target.getCenterX();
			returnValues[1] = target.getCenterY();
			return 2;
		case ROTATION: returnValues[0] = target.getRotation(); return 1;
		case SCALE:
			returnValues[0] = target.getScaleX();
			returnValues[1] = target.getScaleY();
			return 2;

		default: assert false; return -1;
		}
	}

	@Override
	public void setValues(Player target, int tweenType, float[] newValues) {
		switch (tweenType) {
		case POSITION_X: target.setX(newValues[0]); break;
		case POSITION_Y: target.setY(newValues[0]); break;
		case POSITION_XY:
			target.setX(newValues[0]);
			target.setY(newValues[1]);
			break;
		case ROTATION: target.setRotation(newValues[0]); break;
		case SCALE:
			target.setScaleX(newValues[0]);
			target.setScaleY(newValues[1]);
			break;
		default: assert false; break;
		}
	}


}