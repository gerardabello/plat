package com.gerardas.platDesktop.Entities;

import com.badlogic.gdx.graphics.Color;
import com.badlogic.gdx.graphics.g2d.SpriteBatch;
import com.badlogic.gdx.graphics.g2d.TextureRegion;
import com.badlogic.gdx.physics.box2d.Body;
import com.badlogic.gdx.scenes.scene2d.Actor;
import com.gerardas.platDesktop.BoxWorld;
import com.gerardas.platDesktop.GameStage;
import com.gerardas.platDesktop.Level;
import com.gerardas.platDesktop.Objects;
import com.gerardas.platDesktop.Textures;

public class Caixa extends Actor{

	GameStage stage;
	private Body body;
	private TextureRegion tex;
	boolean heavy;



	public Caixa(float x, float y,boolean heavy, GameStage s){

		this.setPosition(x, y);
		this.setSize(64, 64);
		this.setOrigin(getWidth()/2, getHeight()/2);
		this.stage=s;
		this.tex = new TextureRegion(Objects.tex, heavy? 64:0, 64*7, 64, 64);
		this.heavy = heavy;



		body = stage.screen.boxworld.createNewDynBox(x, y, heavy);
		

	}





	@Override
	public void act(float dt){
		super.act(dt);
		setPosition(body.getPosition().x/BoxWorld.scale*64-32, body.getPosition().y/BoxWorld.scale*64-32);
		setRotation(body.getAngle()/2/3.14f*360);
		//TODO potser no es necessari tota lestona
		body.setActive(true);
		body.setAwake(true);	
	}







	@Override
	public void draw(SpriteBatch batch, float arg1) {
		if(Level.DrawShadows){
			batch.setColor(0,0,0,Level.IntShadows);
			
			batch.draw(tex,
					getX()-(float) (-((Level.dxS/1.5)*2)*Math.sin(((stage.cameraAngle+225)/360)*2*Math.PI)),
					getY()-(float) (-((Level.dxS/1.5)*2)*Math.cos(((stage.cameraAngle+225)/360)*2*Math.PI)),
					getOriginX(),
					getOriginY(),
					getWidth(),
					getHeight(),
					getScaleX(),
					getScaleY(),
					getRotation());

			batch.setColor(Color.WHITE);
		}
		
		batch.draw(tex, getX(), getY(), getOriginX(), getOriginY(), getWidth(), getHeight(), getScaleX(), getScaleY(),
				getRotation());
		
		batch.draw(Textures.punt,
				getCenterX(),
				getCenterY());

	}
	
	




	public float getCenterX(){
		return getX()+getWidth()/2;
	}
	
	public float getCenterY(){
		return getY()+getHeight()/2;
	}




}
