package com.gerardas.platDesktop.Entities;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.graphics.Color;
import com.badlogic.gdx.graphics.g2d.SpriteBatch;
import com.badlogic.gdx.scenes.scene2d.Actor;
import com.badlogic.gdx.scenes.scene2d.InputEvent;
import com.badlogic.gdx.scenes.scene2d.InputListener;
import com.gerardas.platDesktop.GameStage;
import com.gerardas.platDesktop.Level;
import com.gerardas.platDesktop.Objects;
import com.gerardas.platDesktop.Textures;
import com.gerardas.platDesktop.Utils;

public class Palanca extends Actor {


	GameStage stage;



	public boolean tocat;
	private int rotacio;



	public Palanca(float x, float y, int irotacio,boolean sentit, GameStage stage){

		this.stage=stage;

		this.setPosition(x, y);
		this.setSize(64, 64);

		tocat=sentit;
		rotacio=irotacio;

		setListeners();




	}



	private void setListeners() {
		 this.addListener(new InputListener() {
	         @Override
	         public boolean touchDown(InputEvent event, float x, float y, int pointer, int button) {
	        	 Gdx.app.log("Palanca", "Clicked");
	        	 checkClicked() ;
	        	 return true;
	         }
	      });

	}





	private void checkClicked() {
		Gdx.app.log("Palanca", Float.toString(Utils.dist(getX(), getY(), this.stage.getPlayer().getX(), this.stage.getPlayer().getY())));
		if(Utils.dist(getX(), getY(), this.stage.getPlayer().getX(), this.stage.getPlayer().getY())<70){
			touch();
			Gdx.app.log("Palanca", "Clicked-Correct");

		}

	}



	public void touch(){

		Level.gir(tocat);

		tocat=!tocat;


		Level.girPos.set(getX()+32,getY()+32);
		//No transformem la variable GirPos a coordenades de Player pk encara no s'ha modificat la variable Orientation




	}



	@Override
	public void draw(SpriteBatch batch, float arg1) {
		if(Level.DrawShadows){
			batch.setColor(0,0,0,Level.IntShadows);



			batch.draw(Objects.tex,
					getX()-(float) (-((Level.dxS/1)*2)*Math.sin(((stage.cameraAngle+225)/360)*2*Math.PI)),
					getY()-(float) (-((Level.dxS/1)*2)*Math.cos(((stage.cameraAngle+225)/360)*2*Math.PI)),
					(32*(tocat? 1:0)+64*rotacio)*2,
					64*2,
					32*2,
					32*2);

			batch.setColor(Color.WHITE);
		}



		batch.draw(Objects.tex,
				getX(),
				getY(),
				(32*(tocat? 1:0)+64*rotacio)*2,
				64*2,
				32*2,
				32*2);
		
		
		batch.draw(Textures.punt,
				getCenterX(),
				getCenterY());


	}
	
	
	
	public float getCenterX(){
		return getX()+getWidth()/2;
	}
	
	public float getCenterY(){
		return getY()+getHeight()/2;
	}




}




