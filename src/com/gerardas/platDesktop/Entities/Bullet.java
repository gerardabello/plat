package com.gerardas.platDesktop.Entities;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.graphics.Color;
import com.badlogic.gdx.graphics.g2d.SpriteBatch;
import com.badlogic.gdx.graphics.g2d.TextureRegion;
import com.badlogic.gdx.physics.box2d.Body;
import com.badlogic.gdx.scenes.scene2d.Actor;
import com.gerardas.platDesktop.BoxWorld;
import com.gerardas.platDesktop.GameStage;
import com.gerardas.platDesktop.Level;
import com.gerardas.platDesktop.Objects;
import com.gerardas.platDesktop.Textures;
import com.gerardas.platDesktop.Utils;

public class Bullet extends Actor{

	GameStage stage;

	Body body;

	boolean wasActive;

	public int frame,Dframe;
	public float frameDt;

	private TextureRegion texr;


	public Bullet(float x, float y, GameStage s){

		this.setPosition(x, y);
		this.setSize(64, 64);
		this.setOrigin(32, 32);
		this.stage=s;

		texr=new TextureRegion(Objects.tex);
		texr.setRegion(
				64*12,
				64*2,
				64,
				64);

		body = stage.screen.boxworld.createBullet();

	}


	public void fire(float x, float y, float a) {


		if(!body.isActive()){
			body.setActive(true);
			body.setTransform(x/64*BoxWorld.scale,y/64*BoxWorld.scale,(a-90-90)/360*2*3.14f);
			this.setPosition(body.getPosition().x/BoxWorld.scale*64, body.getPosition().y/BoxWorld.scale*64);
			body.setLinearVelocity(20*(float)Math.cos((a-90)/360*2*3.14), 20*(float)Math.sin((a-90)/360*2*3.14));
			Gdx.app.log("bullet", "fired");

			texr.setRegion(
					64*12,
					64*2,
					64,
					64);
			setVisible(true);
			frame=0;
			Dframe=0;
		}




	}



	public boolean isActive(){
		return body.isActive();
	}


	@Override
	public void act(float dt){

		if(isVisible()){
			super.act(dt);
			if(isActive()){
				this.setPosition(body.getPosition().x/BoxWorld.scale*64, body.getPosition().y/BoxWorld.scale*64);
				this.setRotation(body.getAngle()/2/3.14f*360);

			}



			if(!isActive()&& wasActive){
				Dframe=4;
			}
			wasActive=isActive();




			frameDt+=dt;

			while(frameDt>=0.03f){
				frameDt-=0.03f;
				if(frame>Dframe){
					frame--;
				}else if (frame<Dframe){
					frame++;
				}

			}
			if(frame==4)setVisible(false);
		}

	}






	@Override
	public void draw(SpriteBatch batch, float arg1) {

		if(isVisible()){


			texr.setRegion(
					64*(12+frame),
					64*2,
					64,
					64);



			batch.draw(texr,
					getX()-32,
					getY()-32,
					32,
					32,
					getWidth(),
					getHeight(),
					getScaleX(),
					getScaleY(),
					getRotation());



		}

	}




	public float getCenterX(){
		return getX()+getWidth()/2;
	}

	public float getCenterY(){
		return getY()+getHeight()/2;
	}




}
