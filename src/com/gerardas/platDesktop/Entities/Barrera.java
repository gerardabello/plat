package com.gerardas.platDesktop.Entities;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.graphics.Color;
import com.badlogic.gdx.graphics.g2d.BitmapFont;
import com.badlogic.gdx.graphics.g2d.SpriteBatch;
import com.badlogic.gdx.scenes.scene2d.Actor;
import com.gerardas.platDesktop.BoxWorld;
import com.gerardas.platDesktop.GameStage;
import com.gerardas.platDesktop.Level;
import com.gerardas.platDesktop.Objects;
import com.gerardas.platDesktop.Tiles;

public class Barrera extends Actor {

GameStage stage;

	public int nCargols;
	public float frame;
	public int h;
	public boolean on;



	public Barrera(float x, float y,int icargols,GameStage s){

		this.setPosition(x, y);
		this.stage=s;

		frame=0;
		nCargols=icargols;
		buscarH:
			for (int i = 1; i < 16; i++) {  //busquem l'alcada
				if(Tiles.col(Level.idPoint(x,y+Level.map.tileHeight*i))){
					h=i;
					Gdx.app.log("h", Integer.toString(i));
					break buscarH;
				}
			}
		this.setSize(64, 64*h);

		on=(stage.nCD<nCargols);

		if(on){
			for (int i = 0; i < h; i++) {
				Level.map.layers.get(Level.layers[0]).tiles[Level.posPoint(x, y)[0]-i][Level.posPoint(x, y)[1]]=17;
				stage.screen.boxworld.createNewTile((((int)x)/64+0.5f)*BoxWorld.scale,((int)y/64+0.5f+i)*BoxWorld.scale);
			}
		}
		
	}

	public void create(float x, float y,int icargols){
		
	}


	@Override
	public void act(float dt){
		super.act(dt);
		frame=frame+dt*40;
		if(frame>Level.map.tileHeight){
			frame=frame-Level.map.tileHeight;
		}

	}
	
	
	public void drawText(SpriteBatch batch, BitmapFont font){

			font.draw(batch, Integer.toString(nCargols), getX()-font.getBounds(Integer.toString(nCargols)).width/2, getY()+((h-1)*Level.map.tileHeight)/2+font.getCapHeight()/2);	
		
	}
	

	@Override
	public void draw(SpriteBatch batch, float arg1) {
		if(Level.DrawShadows){
			if(on){
				batch.setColor(0,0,0,Level.IntShadows/2);


				for (int i = 0; i < h; i++) {
					batch.draw(Objects.tex,
							getX()-(float) (-((Level.dxS/2)*2)*Math.sin(((stage.cameraAngle+225)/360)*2*Math.PI)),
							getY()+i*32*2-(float) (-((Level.dxS/2)*2)*Math.cos(((stage.cameraAngle+225)/360)*2*Math.PI)),
							0*2,
							128*2+(int)frame,
							32*2,
							32*2);
				}

			}
			batch.setColor(0,0,0,Level.IntShadows);

			batch.draw(Objects.tex,
					getX()-(float) (-((Level.dxS/1)*2)*Math.sin(((stage.cameraAngle+225)/360)*2*Math.PI)),
					getY()+0*32*2-(float) (-((Level.dxS/1)*2)*Math.cos(((stage.cameraAngle+225)/360)*2*Math.PI)),
					64*2,
					128*2,
					32*2,
					32*2);

			batch.draw(Objects.tex,
					getX()-(float) (-((Level.dxS/1)*2)*Math.sin(((stage.cameraAngle+225)/360)*2*Math.PI)),
					getY()+(h-1)*32*2-(float) (-((Level.dxS/1)*2)*Math.cos(((stage.cameraAngle+225)/360)*2*Math.PI)),
					96*2,
					128*2,
					32*2,
					32*2);



			batch.setColor(Color.WHITE);
		}



		if(on){
			for (int i = 0; i < h; i++) {
				batch.draw(Objects.tex,
						getX(),
						getY()+i*32*2,
						0*2,
						128*2+(int)frame,
						32*2,
						32*2);
			}
		}

		batch.draw(Objects.tex,
				getX(),
				getY()+0*32*2,
				64*2,
				128*2,
				32*2,
				32*2);

		batch.draw(Objects.tex,
				getX(),
				getY()+(h-1)*32*2,
				96*2,
				128*2,
				32*2,
				32*2);
		if(on){
			for (int i = 0; i < h; i++) {
				batch.draw(Objects.tex,
						getX(),
						getY()+i*32*2,
						32*2,
						128*2+(int)frame,
						32*2,
						32*2);
			}
		}
		
	}




}
