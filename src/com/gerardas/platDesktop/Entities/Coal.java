package com.gerardas.platDesktop.Entities;

import com.badlogic.gdx.graphics.Color;
import com.badlogic.gdx.graphics.g2d.SpriteBatch;
import com.badlogic.gdx.scenes.scene2d.Actor;
import com.gerardas.platDesktop.GameStage;
import com.gerardas.platDesktop.Level;
import com.gerardas.platDesktop.Objects;
import com.gerardas.platDesktop.Textures;
import com.gerardas.platDesktop.Utils;

public class Coal extends Actor {
	GameStage stage;

	public int frame;
	public float frameDt;

	public Coal(float x, float y, GameStage s) {
		this.setPosition(x, y);
		this.setSize(64, 64);
		this.stage=s;


		frame = 0;

	}

	@Override
	public void act(float dt) {
		super.act(dt);

		frameDt += dt;

		while (frameDt >= 0.1f) {
			frameDt -= 0.1f;
			frame++;
			if (frame == 10) {
				frame = 0;
			}
		}
		
		if(CollisionPlayer()){
			stage.nCO++;
			this.setVisible(false);
		}
		
	}




	private boolean CollisionPlayer() {
		return Utils.dist(getCenterX(), getCenterY(), ((GameStage)getStage()).getPlayer().getCenterX(), ((GameStage)getStage()).getPlayer().getCenterY())<40;
	}


	@Override
	public void draw(SpriteBatch batch, float arg1) {
		if (Level.DrawShadows) {
			batch.setColor(0, 0, 0, Level.IntShadows);
			batch.draw(
					Objects.tex,
					getX()
							- (float) (-((Level.dxS / 1.5) * 2) * Math
									.sin(((stage.cameraAngle + 225) / 360) * 2
											* Math.PI)),
					getY()
							- (float) (-((Level.dxS / 1.5) * 2) * Math
									.cos(((stage.cameraAngle + 225) / 360) * 2
											* Math.PI)), 32 * frame * 2,
					6 * 32 * 2, 32 * 2, 32 * 2);

			batch.setColor(Color.WHITE);
		}
		batch.draw(Objects.tex, getX(), getY(), 32 * frame * 2, 6 * 32 * 2,
				32 * 2, 32 * 2);

		batch.draw(Textures.punt, getCenterX(), getCenterY());
	}
	
	public float getCenterX(){
		return getX()+getWidth()/2;
	}
	
	public float getCenterY(){
		return getY()+getHeight()/2;
	}


}
