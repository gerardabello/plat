package com.gerardas.platDesktop.Entities;

import com.badlogic.gdx.graphics.Color;
import com.badlogic.gdx.graphics.g2d.SpriteBatch;
import com.badlogic.gdx.graphics.g2d.TextureRegion;
import com.badlogic.gdx.scenes.scene2d.Actor;
import com.gerardas.platDesktop.GameStage;
import com.gerardas.platDesktop.Level;
import com.gerardas.platDesktop.Objects;
import com.gerardas.platDesktop.Textures;
import com.gerardas.platDesktop.Utils;

public class Electricitat extends Actor{

	GameStage stage;

	public int frame;
	public float frameDt;
	public float limitFrame;
	TextureRegion texr;



	public Electricitat(float x, float y, int ir, GameStage s){

		this.setPosition(x, y);
		this.setSize(64, 64);
		this.setRotation(ir);
		this.stage=s;
		
		this.texr=new TextureRegion(Objects.tex);

		frame=(int)(Math.random()*9);
		limitFrame=0.09f+0.02f-(float)Math.random()*0.04f;

	}





	@Override
	public void act(float dt){
		super.act(dt);

		frameDt+=dt;

		while(frameDt>=limitFrame){
			frameDt-=limitFrame;
			frame++;
			if(frame==10){
				frame=0;
			}
		}
		
		
		if(CollisionPlayer())stage.mort();
		
	}




	private boolean CollisionPlayer() {
		return Utils.dist(getCenterX(), getCenterY(), ((GameStage)getStage()).getPlayer().getCenterX(), ((GameStage)getStage()).getPlayer().getCenterY())<32;
	}





	@Override
	public void draw(SpriteBatch batch, float arg1) {
		
		texr.setRegion(
				64*frame,
				64*8,
				64,
				64);
		
		
		if(Level.DrawShadows){
			batch.setColor(0,0,0,Level.IntShadows);

			
			batch.draw(texr,
					getX()-(float) (-((Level.dxS/1.5)*2)*Math.sin(((stage.cameraAngle+225)/360)*2*Math.PI)),
					getY()-(float) (-((Level.dxS/1.5)*2)*Math.cos(((stage.cameraAngle+225)/360)*2*Math.PI)),
					32,
					32,
					getWidth(),
					getHeight(),
					getScaleX(),
					getScaleY(),
					getRotation());
			

			batch.setColor(Color.WHITE);
		}
		
		batch.draw(texr,
				getX(),
				getY(),
				32,
				32,
				getWidth(),
				getHeight(),
				getScaleX(),
				getScaleY(),
				getRotation());
//		
//		batch.draw(Textures.punt,
//				getCenterX(),
//				getCenterY());

	}




	public float getCenterX(){
		return getX()+getWidth()/2;
	}
	
	public float getCenterY(){
		return getY()+getHeight()/2;
	}




}
