package com.gerardas.platDesktop.Entities;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.graphics.Color;
import com.badlogic.gdx.graphics.g2d.BitmapFont;
import com.badlogic.gdx.graphics.g2d.SpriteBatch;
import com.badlogic.gdx.scenes.scene2d.Actor;
import com.badlogic.gdx.scenes.scene2d.InputEvent;
import com.badlogic.gdx.scenes.scene2d.InputListener;
import com.gerardas.platDesktop.GameStage;
import com.gerardas.platDesktop.Level;
import com.gerardas.platDesktop.Objects;
import com.gerardas.platDesktop.Tiles;
import com.gerardas.platDesktop.Utils;

public class Porta extends Actor {

	GameStage stage;

	BitmapFont font;

	public int level,h;


	public Porta(float x, float y,int ilevel,GameStage s){
		this.setPosition(x, y);
		this.setSize(64, 64);
		this.stage=s;

		font=stage.screen.ui.font;


		h=16;
		buscarH:
			for (int i = 3; i < 16; i++) {  //busquem l'alcada
				if(Tiles.col(Level.idPoint(x,y-Level.map.tileHeight*i))){
					h=i;
					break buscarH;
				}
			}


		level=ilevel;
		setListeners();
	}


	private void setListeners() {
		this.addListener(new InputListener() {
			@Override
			public boolean touchDown(InputEvent event, float x, float y, int pointer, int button) {
				Gdx.app.log("porta", "Clicked");
				checkClicked() ;
				return true;
			}
		});

	}


	private void checkClicked() {
		Gdx.app.log("Palanca", Float.toString(Utils.dist(getX(), getY(), this.stage.getPlayer().getX(), this.stage.getPlayer().getY())));
		if(Utils.dist(getX(), getY(), this.stage.getPlayer().getX(), this.stage.getPlayer().getY())<70){
			Level.load(level);
			Gdx.app.log("Palanca", "Clicked-Correct");

		}

	}






	public void drawText(SpriteBatch batch, BitmapFont font){


		font.draw(batch, Integer.toString(level), getX()-font.getBounds(Integer.toString(level)).width/2, getY()-Level.map.tileHeight+font.getCapHeight()/2);	


	}



	@Override
	public void draw(SpriteBatch batch, float palpha) {

		if(Level.DrawShadows){
			batch.setColor(0,0,0,Level.IntShadows);



			batch.draw(Objects.tex,
					getX()-(float) (-((Level.dxS/1)*2)*Math.sin(((stage.cameraAngle+225)/360)*2*Math.PI)),
					getY()-(float) (-((Level.dxS/1)*2)*Math.cos(((stage.cameraAngle+225)/360)*2*Math.PI)),
					0*2,
					96*2,
					32*2,
					32*2);



			for (int i = 1; i < h; i++) {
				batch.draw(Objects.tex,
						getX()-(float) (-((Level.dxS/1)*2)*Math.sin(((stage.cameraAngle+225)/360)*2*Math.PI)),
						getY()-i*32*2-(float) (-((Level.dxS/1)*2)*Math.cos(((stage.cameraAngle+225)/360)*2*Math.PI)),
						64*2,
						96*2,
						32*2,
						32*2);
			}



			batch.setColor(Color.WHITE);
		}



		batch.draw(Objects.tex,
				getX(),
				getY(),
				32*2,
				96*2,
				32*2,
				32*2);

		for (int i = 1; i < h; i++) {
			batch.draw(Objects.tex,
					getX(),
					getY()-i*32*2,
					64*2,
					96*2,
					32*2,
					32*2);
		}



	}


	public void drawCartell(SpriteBatch batch, float arg1) {

		font.draw(batch, Integer.toString(level),getX()+32-font.getBounds(Integer.toString(level)).width/2, getY()-32+font.getBounds(Integer.toString(level)).height/2);

	}




}
