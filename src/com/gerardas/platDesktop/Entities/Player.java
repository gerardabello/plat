package com.gerardas.platDesktop.Entities;

import aurelienribon.tweenengine.Timeline;
import aurelienribon.tweenengine.Tween;
import aurelienribon.tweenengine.TweenEquation;
import aurelienribon.tweenengine.TweenEquations;
import aurelienribon.tweenengine.TweenManager;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.Input.Keys;
import com.badlogic.gdx.graphics.Color;
import com.badlogic.gdx.graphics.Texture;
import com.badlogic.gdx.graphics.Texture.TextureFilter;
import com.badlogic.gdx.graphics.g2d.SpriteBatch;
import com.badlogic.gdx.graphics.g2d.TextureRegion;
import com.badlogic.gdx.math.EarClippingTriangulator;
import com.badlogic.gdx.math.Vector2;
import com.badlogic.gdx.physics.box2d.Body;
import com.badlogic.gdx.scenes.scene2d.Actor;
import com.gerardas.platDesktop.BoxWorld;
import com.gerardas.platDesktop.GameStage;
import com.gerardas.platDesktop.Level;
import com.gerardas.platDesktop.Objects;
import com.gerardas.platDesktop.Textures;
import com.gerardas.platDesktop.Entities.TweenAccessors.PlayerAccessor;


public class Player extends Actor {

	Body body;

	private TweenManager tweenManager;


GameStage stage;





	private  int frame=0;
	private  float frameDt=0;


	private TextureRegion texr;

	private  boolean dir=false;
	private  int state=2;
	//0. running
	//1. quiet
	//2. saltant
	//3. entrant
	//4.sortint
	//10. mort

	public  Vector2 positionL;

	public  Vector2 positionWorld;



	private Vector2 velocity;
	
	
	
	//estats joc
	


	public Player(GameStage s){
		
		this.setSize(64, 64);

		stage=s;
		
		tweenManager = new TweenManager();


		//tex.setFilter(TextureFilter.Linear, TextureFilter.Linear);
		texr=new TextureRegion(Objects.tex);
		texr.setRegionHeight(64);
		texr.setRegionWidth(64);

		//		if(Camera.factor!=1f && Camera.factor!=.5f && Camera.factor!=2){
		//			tex.setFilter(TextureFilter.Linear,TextureFilter.Linear);
		//		}



		positionWorld= new Vector2(0,0);








	}


	@Override
	public void act(float dt){
		super.act(dt);




		tweenManager.update(dt);




		




		body= stage.screen.boxworld.player;

		velocity=body.getLinearVelocity().mul(BoxWorld.scale).rotate(-body.getAngle()/2/3.14f*360);



		position_upd(dt);


		//		Gdx.app.log("pos", Float.toString(position.x)+","+Float.toString(position.y));
		//		Gdx.app.log("velocity", Float.toString(velX));
		//		Gdx.app.log("acceleration", Float.toString(acY));



		calcAnimation(dt);




	}


	private void calcAnimation(float dt) {
		frameDt+=dt*((float)Math.abs(velocity.x/40.0));

		while(frameDt>=0.04){
			frameDt-=0.04;
			frame++;
			if(frame==10){
				frame=0;
			}
		}
		
		
		
//		if(velocity.y<-250){
//			preparat_aplastar=true;
//		}
//		
//		if(preparat_aplastar && velocity.y >-1){
//			
//			preparat_aplastar=false;
//			
//
//			
//			float ax=getX();
//			float ay=getY();
//			
//			dir=false;
//			Timeline.createSequence()
//			
//		    .push(Tween.set(this, PlayerAccessor.SCALE).target(1.5f,0.5f))
//		    .push(Tween.set(this, PlayerAccessor.POSITION_XY).target(ax,ay-30))
//		    .push(Tween.to(this, PlayerAccessor.SCALE, 1f).target(1.45f,0.55f))
//		    .beginParallel()
//		    	.push(Tween.to(this, PlayerAccessor.SCALE, .1f).target(1,1))
//		    	.push(Tween.to(this, PlayerAccessor.ROTATION, .2f).target(0))
//		    	.beginSequence()
//		    		.push(Tween.to(this, PlayerAccessor.POSITION_XY, .07f).target(ax+1,ay+30).ease(TweenEquations.easeInBack))
//		    		.push(Tween.to(this, PlayerAccessor.POSITION_XY, .07f).target(ax+2,ay+45))
//		    		.push(Tween.to(this, PlayerAccessor.POSITION_XY, .07f).target(ax+3,ay+45))
//		    		.push(Tween.to(this, PlayerAccessor.POSITION_XY, .07f).target(ax+4,ay+30))
//		    		.push(Tween.to(this, PlayerAccessor.POSITION_XY, .07f).target(ax+5,ay))
//		    	.end()
//		    .end()
//		    
//		    .start(tweenManager);
//			
//			
//		}

	}








	public  int getState(){
		return state;
	}



	private  boolean check_pinxus() {
		//		Gdx.app.log("Colisionid",Integer.toString(Level.idPoint(position.x-(sqL-5*2), position.y-sqU)));
		//		int id = Level.idPoint(position.x-(sqL-5*2), position.y-sqU);
		//		return id == 92 || id == 110;
		return false;
	}

	public  void position_upd(float dt){


		Vector2 boxPosition = body.getPosition().div(BoxWorld.scale/64.0f);
		//position=boxPosition;







		if(tweenManager.size()==0)setRotation(-stage.cameraAngle);


		setPosition(
				body.getPosition().x/BoxWorld.scale*64,
				body.getPosition().y/BoxWorld.scale*64);

	}




	public  void spawn(float x, float y){
		body= stage.screen.boxworld.player;
		
		stage.screen.cam.position.set(x, y, 0);


		if(true){
			dir=false;
			Timeline.createSequence()
			.pushPause(1.0f)
			.push(Tween.set(this, PlayerAccessor.POSITION_XY).target(x-25,y+300))
			.push(Tween.set(this, PlayerAccessor.ROTATION).target(180))
		    .push(Tween.to(this, PlayerAccessor.POSITION_XY, .4f).target(x-25,y-20).ease(TweenEquations.easeInQuad))
		    .push(Tween.set(this, PlayerAccessor.SCALE).target(1.2f,0.8f))
		    .beginParallel()
		    	.push(Tween.to(this, PlayerAccessor.ROTATION, 2).target(177).ease(TweenEquations.easeInBack))
		  		  .push(Tween.to(this, PlayerAccessor.SCALE, 2).target(1.1f,0.9f).ease(TweenEquations.easeInBack))

		    .end()
		    .beginParallel()
		    	.push(Tween.to(this, PlayerAccessor.SCALE, .1f).target(1,1))
		    	.push(Tween.to(this, PlayerAccessor.ROTATION, .2f).target(0))
		    	.beginSequence()
		    		.push(Tween.to(this, PlayerAccessor.POSITION_XY, .07f).target(x-20,y+30))
		    		.push(Tween.to(this, PlayerAccessor.POSITION_XY, .07f).target(x-15,y+45))
		    		.push(Tween.to(this, PlayerAccessor.POSITION_XY, .07f).target(x-10,y+45))
		    		.push(Tween.to(this, PlayerAccessor.POSITION_XY, .07f).target(x-5,y+30))
		    		.push(Tween.to(this, PlayerAccessor.POSITION_XY, .07f).target(x,y))
		    	.end()
		    .end()
		    
		    .start(tweenManager);

		}else{

			body.setTransform(x/64*BoxWorld.scale, y/64*BoxWorld.scale, 0);

			body.setLinearVelocity(0, 0);

		}

	}






	@Override
	public void draw(SpriteBatch batch, float palpha) {







		if(Math.abs(velocity.x)>0.4){dir = velocity.x<0;}
		if(Math.abs(velocity.x)>20){
			texr.setRegion(
					64*frame,
					((dir? 64:0))+64*14,
					64,
					64);


		}else{

			if(dir){
				texr.setRegion(
						576,
						64*15,
						64,
						64);
			}else{
				texr.setRegion(
						0,
						64*14,
						64,
						64);
			}

		}





		if(Level.DrawShadows){
			batch.setColor(0,0,0,Level.IntShadows);

			batch.draw(texr,
					getX()-32-(float) (-((Level.dxS/1.5)*2)*Math.sin(((stage.cameraAngle+225)/360)*2*Math.PI)),
					getY()-32-(float) (-((Level.dxS/1.5)*2)*Math.cos(((stage.cameraAngle+225)/360)*2*Math.PI)),
					32,
					32,
					getWidth(),
					getHeight(),
					getScaleX(),
					getScaleY(),
					getRotation());

			batch.setColor(Color.WHITE);
		}

		batch.draw(texr,
				getX()-32,
				getY()-32,
				32,
				32,
				getWidth(),
				getHeight(),
				getScaleX(),
				getScaleY(),
				getRotation());






		batch.draw(Textures.punt,
				getX(),
				getY());

		batch.draw(Textures.punt,
				((GameStage)getStage()).screen.boxworld.player.getPosition().x/BoxWorld.scale*64,
				((GameStage)getStage()).screen.boxworld.player.getPosition().y/BoxWorld.scale*64);


	}



	
	public void setRotation(float r){
		super.setRotation(r);
		body.setTransform(body.getPosition().x, body.getPosition().y, r/360*2*3.14f);		
	}
	
	public float getCenterX() {
		return getX();
	}
	public float getCenterY() {
		return getY();
	}
	
	
	public void setX(float x){
		super.setX(x);
		body.setTransform(x*BoxWorld.scale/64, body.getPosition().y, body.getAngle());		
	}
	


	public void setY(float y){
		super.setY(y);
		body.setTransform(body.getPosition().x, y*BoxWorld.scale/64, body.getAngle());		
	}


	public boolean isAnimating() {
		return tweenManager.size()!=0;
	}

}
