package com.gerardas.platDesktop;

public class Utils {
	
	
	
	public static float distSquared(float x1, float y1, float x2, float y2){
		return (x2-x1)*(x2-x1)+(y2-y1)*(y2-y1);
	}
	
	public static float dist(float x1, float y1, float x2, float y2){
		return (float)Math.sqrt(distSquared(x1, y1, x2, y2));
	}

}
