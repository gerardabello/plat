package com.gerardas.platDesktop;

import com.badlogic.gdx.backends.lwjgl.LwjglApplication;
import com.badlogic.gdx.backends.lwjgl.LwjglApplicationConfiguration;


public class MainDesktop {
	public static void main (String[] argv) {
		LwjglApplicationConfiguration c = new LwjglApplicationConfiguration();
		c.vSyncEnabled=true;
		c.height=480;
		c.width=800;
		c.title="Plat";
		new LwjglApplication(new Plat(), c);
	}
}
