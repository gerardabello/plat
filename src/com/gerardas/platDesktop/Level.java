package com.gerardas.platDesktop;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.graphics.Color;
import com.badlogic.gdx.graphics.Texture;
import com.badlogic.gdx.graphics.g2d.SpriteBatch;
import com.badlogic.gdx.graphics.g2d.tiled.SimpleTileAtlas;
import com.badlogic.gdx.graphics.g2d.tiled.TileMapRenderer;
import com.badlogic.gdx.graphics.g2d.tiled.TiledLoader;
import com.badlogic.gdx.graphics.g2d.tiled.TiledMap;
import com.badlogic.gdx.math.Vector2;
import com.badlogic.gdx.math.Vector3;

public class Level {

	public static GameScreen screen;

	public static final boolean DrawShadows = true;
	public static final boolean DrawSmoke = true;

	public static final float IntShadows = 0.13f;

	public static int cWorld, cLevel;

	public static int tempx, tempy;

	public static final int dxS = 10;

	public static int[] layers, objects, back, sombra,pipes;

	public static Texture ptex;

	public static TiledMap map;
	public static SimpleTileAtlas atlas;
	public static TileMapRenderer mapR;
	public static int orientation = 0;
	public static boolean transition = false;
	public static boolean transitionEs = false;

	public static int[] tempPos;
	private static float temp;
	private static Vector2 tempVector, tempVector2;
	public static Vector2 girPos;
	private static float tempAC;

	public static Vector2 spawn;

	public static int faseGir;
	private static int stageSpawn;
	public static boolean spawnEntering;
	private static float spawnDt;


	public static void ini_level() {
		ptex = new Texture(Gdx.files.internal("data/img/punt.png"));

		layers = new int[1];
		objects = new int[1];
		sombra = new int[1];
		back = new int[1];
		pipes = new int[1];

		tempPos = new int[2];

		tempVector = new Vector2(0, 0);
		tempVector2 = new Vector2(0, 0);
		girPos = new Vector2(0, 0);
		spawn = new Vector2(0, 0);

		stageSpawn = 0;
		spawnEntering = false;
		spawnDt = 0;

	}

	public static void gir(boolean es) {
		if (!transition) {
			transition = true;

			transitionEs = es;

			tempAC = 0;
			faseGir = 0;
		}
	}

	public static void gir_upd(float dt) {
		// Gdx.app.log("CameraAngle", Float.toString(screen.stage.cameraAngle));
		if (transition) {

			screen.boxworld.enabled = false;

			switch (faseGir) {
			case 0:
				tempAC += dt;

				if (tempAC >= 1) {

					faseGir++;

					Level.orientation += (transitionEs ? -1 : 1);
					// screen.stage.cameraAngle+=90;
					// screen.cam.rotate(-90, 0, 0, 1);

					if (Level.orientation == 4) {
						Level.orientation = 0;
					}
					if (Level.orientation == -1) {
						Level.orientation = 3;
					}



					Level.levelToPlayer(Level.girPos); // La posici� que s'ha
														// assignat desde la
														// palanca, la convertim
														// a coordenades de
														// screen.stage.player
														// ara perque ja s'ha
														// canviat el valor de
														// Orientation

					// tempVector.set(map.tileHeight*rx(screen.stage.player.position.x,screen.stage.player.position.y),
					// map.tileHeight*ry(screen.stage.player.position.x,screen.stage.player.position.y));
					// substituir per la posicio de la casella amb el
					// interruptor

					tempAC = 0;
				}
				break;

			case 1:
				temp = 90 * dt * (transitionEs ? -1 : 1);
				tempAC += temp;

				screen.stage.cameraAngle += temp;
				screen.cam.rotate(-temp, 0, 0, 1);

				if (tempAC * (transitionEs ? -1 : 1) > 90) {
					screen.boxworld.enabled = true;
					transition = false;

					screen.stage.cameraAngle -= tempAC;
					screen.cam.rotate(tempAC, 0, 0, 1);

					screen.stage.cameraAngle += 90 * (transitionEs ? -1 : 1);
					screen.cam.rotate(-90 * (transitionEs ? -1 : 1), 0, 0, 1);

				}

				break;
			}

		}
	}

	public static void load(int level) {
		stageSpawn = 0;
		orientation = 0;


		// ResetCamera
		screen.cam.rotate(screen.stage.cameraAngle, 0, 0, 1);
		screen.stage.cameraAngle = 0;

		cLevel = level;

		if (level == 0) {
			map = TiledLoader.createMap(Gdx.files
					.internal("data/world/world.tmx"));
			// atlas=new
			// SimpleTileAtlas(map,Gdx.files.internal("img/tilemap.png"));
		} else {
			map = TiledLoader.createMap(Gdx.files.internal("data/world/level_"
					+ "1" + "_" + Integer.toString(level) + ".tmx"));
		}

		// triar layer
		for (int i = 0; i < map.layers.size(); i++) {
			if (map.layers.get(i).name.equals("tiles")) {
				layers[0] = i;
			} else if (map.layers.get(i).name.equals("objects")) {
				objects[0] = i;
			} else if (map.layers.get(i).name.equals("back")) {
				back[0] = i;
			} else if (map.layers.get(i).name.equals("sombra")) {
				sombra[0] = i;
			} else if (map.layers.get(i).name.equals("pipes")) {
				pipes[0] = i;
			}
		}

		// crear sombres
		for (int i = 0; i < Level.map.height; i++) {
			for (int j = 0; j < Level.map.width; j++) {
				if (Level.map.layers.get(Level.layers[0]).tiles[i][j] == 92) {
					map.layers.get(sombra[0]).tiles[i][j] = 166;
				} else if (Level.map.layers.get(Level.layers[0]).tiles[i][j] != 0) {
					map.layers.get(sombra[0]).tiles[i][j] = 151;
				}
			}
		}

		atlas = new SimpleTileAtlas(map, Gdx.files.internal("data/world/"));

		mapR = new TileMapRenderer(map, atlas,
				(int) (screen.cam.viewportWidth / 16.0f),
				(int) (screen.cam.viewportWidth / 16.0f));

		//
		// if(Camera.factor!=1f && Camera.factor!=.5f && Camera.factor!=2){
		// mapR.getAtlas().getRegion(1).getTexture().setFilter(TextureFilter.Linear,
		// TextureFilter.Linear);
		// mapR.getAtlas().getRegion(2).getTexture().setFilter(TextureFilter.Linear,
		// TextureFilter.Linear);
		// mapR.getAtlas().getRegion(3).getTexture().setFilter(TextureFilter.Linear,
		// TextureFilter.Linear);
		// mapR.getAtlas().getRegion(4).getTexture().setFilter(TextureFilter.Linear,
		// TextureFilter.Linear);
		// mapR.getAtlas().getRegion(5).getTexture().setFilter(TextureFilter.Linear,
		// TextureFilter.Linear);
		// }
		//

		// Spawn
		searchSpawn: for (int i = 0; i < Level.map.height; i++) {
			for (int j = 0; j < Level.map.width; j++) {
				if (Level.map.layers.get(Level.objects[0]).tiles[i][j] == 45) {

					spawn.set(j*map.tileWidth+(map.tileWidth/2),(map.height-i-1)*map.tileHeight+(map.tileHeight/2));
					break searchSpawn;
				}
			}
		}

		screen.boxworld.setTiles(map.layers.get(Level.layers[0]).tiles);

		Objects.load(screen.stage);

		// if(level==0 && screen.stage.player.positionWorld.x!=0){ //surt d'un
		// nivell
		// screen.stage.player.surt();
		// }else{
		// spawnEntering=true;
		// spawnDt=0;
		// stageSpawn=0;
		// }

		screen.stage.player.spawn(spawn.x, spawn.y);

	}

	

	public static void levelToPlayer(Vector2 vector) {

		if (Level.orientation > 0) {
			temp = vector.y;
			vector.y = vector.x;
			vector.x = map.height * (map.tileHeight) - temp; // TODO

		}

		if (Level.orientation > 1) {
			temp = vector.y;
			vector.y = vector.x;
			vector.x = map.height * (map.tileHeight) - temp;// TODO

		}

		if (Level.orientation > 2) {
			temp = vector.y;
			vector.y = vector.x;
			vector.x = map.height * (map.tileHeight) - temp;// TODO

		}

		temp = 0;
	}

	public static void playerToLevel(Vector2 vector) {

		if (Level.orientation > 0) {
			temp = vector.y;
			vector.y = map.height * Level.map.tileHeight - vector.x;// TODO
			vector.x = temp;

		}

		if (Level.orientation > 1) {
			temp = vector.y;
			vector.y = map.height * Level.map.tileHeight - vector.x;// TODO
			vector.x = temp;

		}

		if (Level.orientation > 2) {
			temp = vector.y;
			vector.y = map.height * Level.map.tileHeight - vector.x;// TODO
			vector.x = temp;

		}

		temp = 0;
	}

	public static void playerToLevel3(Vector3 vector) {

		if (Level.orientation > 0) {
			temp = vector.y;
			vector.y = map.height * Level.map.tileHeight - vector.x;// TODO
			vector.x = temp;

		}

		if (Level.orientation > 1) {
			temp = vector.y;
			vector.y = map.height * Level.map.tileHeight - vector.x;// TODO
			vector.x = temp;

		}

		if (Level.orientation > 2) {
			temp = vector.y;
			vector.y = map.height * Level.map.tileHeight - vector.x;// TODO
			vector.x = temp;

		}

		temp = 0;
	}

	public static int idPoint(Vector2 vector) {

		return idPoint(vector.x, vector.y);

	}

	public static int idPoint(float posX, float posY) {
		return idPoint_d(posPoint(posX, posY));
	}

	public static int idPoint_d(int[] pos) {
		return idPoint_d(pos[0], pos[1]);
	}

	public static int idPoint_d(int posX, int posY) {

		if (posX >= 0 && posX < map.height && posY >= 0 && posY < map.height) {
			return Level.map.layers.get(layers[0]).tiles[posX][posY];
		} else {
			return 1; // si esta fora, retorna 1 que es sòlid
		}
	}

	public static int[] posPoint(float posX, float posY) {
		return posPoint(posX, posY, 0, 0);
	}

	public static int[] posPoint(float posX, float posY, int addX, int addY) {
		tempx = (((int) posX) / map.tileWidth) + 0;
		tempy = (Level.map.height - (((int) posY) / map.tileWidth)) - 1;

		tempx += addX;
		tempy += addY;

		if (orientation == 0) {
			tempPos[0] = tempy;
			tempPos[1] = tempx;

		} else if (orientation == 1) {
			tempPos[0] = tempx;
			tempPos[1] = Level.map.height - tempy - 1;

		} else if (orientation == 2) {
			tempPos[0] = Level.map.height - tempy - 1;
			tempPos[1] = Level.map.height - tempx - 1;

		} else if (orientation == 3) {
			tempPos[0] = Level.map.height - tempx - 1;
			tempPos[1] = tempy;

		} else {
			tempPos[0] = -1;
			tempPos[1] = -1;
		}

		return tempPos;

	}

	public static int idPoint(int posX, int posY) {

		return idPoint((float) posX, (float) posY);

	}

	public static int ry(float posX, float posY) {

		tempx = (((int) posX) / map.tileWidth) + 0;
		tempy = (Level.map.height - (((int) posY) / map.tileHeight)) - 1;

		return Level.map.height - tempy - 1;

	}

	public static int rx(float posX, float posY) {

		tempx = (((int) posX) / map.tileWidth) + 0;
		tempy = (Level.map.height - (((int) posY) / map.tileHeight)) - 1;

		return tempx;

	}

}
