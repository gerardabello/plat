package com.gerardas.platDesktop;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.Screen;
import com.badlogic.gdx.Input.Keys;
import com.badlogic.gdx.scenes.scene2d.Group;
import com.badlogic.gdx.scenes.scene2d.Stage;
import com.gerardas.platDesktop.Entities.Player;

public class GameStage extends Stage {
	
	public float cameraAngle=0;
	
	//parent
	public GameScreen screen;
	
	
	//entities
	Player player;
	
	public Group cargols,coals,palanques,barreres,portes, caixes,electricitats, lamps, turretas,bullets;

	
	public int nCD=4;
	public int nCO=4;
	
	
	
	public boolean mort,guanyat;
	
	



	public GameStage(float width, float height, boolean stretch, GameScreen s) {
		super(width, height, stretch);
		
		screen=s;
		
		setPlayer();
		setGroups();	

	}
	
	
	
	@Override
	public void act(float dt){
		
		
		if(!checkState())super.act(dt);
		

	}
	
	
	
	private boolean checkState() {
		if(guanyat){
			guanyat=false;
			nCD++;
			Level.load(0);
			return true;
		}else if(mort){
			mort=false;
			Level.load(0);
			return true;
		}else{
			return false;
		}		
	}



	public void guanyat(){
		guanyat=true;
	}
	


	
	
	
	private void setGroups() {
		
		caixes=new Group();
		caixes.setName("caixes");
		addActor(caixes);
		
		cargols=new Group();
		cargols.setName("cargols");
		addActor(cargols);
		
		coals=new Group();
		coals.setName("coals");
		addActor(coals);

		palanques=new Group();
		palanques.setName("palanques");
		addActor(palanques);

		barreres=new Group();
		barreres.setName("barreres");
		addActor(barreres);

		portes=new Group();
		portes.setName("portes");
		addActor(portes);
		
		electricitats=new Group();
		electricitats.setName("electricitats");
		addActor(electricitats);
		
		lamps=new Group();
		lamps.setName("lamps");
		addActor(lamps);
		
		bullets=new Group();
		bullets.setName("bullets");
		addActor(bullets);
		
		turretas=new Group();
		turretas.setName("turretas");
		addActor(turretas);
		
		

		

		
	}



	public void setPlayer(){
		player=new Player(this);
		this.addActor(player);
	}
	
	
	public Player getPlayer(){
		return player;
	}
	
	




	public void mort() {
		mort=true;
		
	}

	

}
