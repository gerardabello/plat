package com.gerardas.platDesktop;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.graphics.Texture;
import com.gerardas.platDesktop.Entities.Barrera;
import com.gerardas.platDesktop.Entities.Bullet;
import com.gerardas.platDesktop.Entities.Caixa;
import com.gerardas.platDesktop.Entities.Cargol;
import com.gerardas.platDesktop.Entities.Coal;
import com.gerardas.platDesktop.Entities.Electricitat;
import com.gerardas.platDesktop.Entities.Lamp;
import com.gerardas.platDesktop.Entities.Palanca;
import com.gerardas.platDesktop.Entities.Porta;
import com.gerardas.platDesktop.Entities.Turreta;

public class Objects {

	public static Texture tex;




	public static void ini(){
		
		tex = new Texture(Gdx.files.internal("data/img/obj2.png"));
		
//		
//		if(Camera.factor!=1f && Camera.factor!=.5f && Camera.factor!=2){
//			tex.setFilter(TextureFilter.Linear,TextureFilter.Linear);
//		}



	}


	public static void load(GameStage s){
		

		
		s.cargols.clear();
		s.coals.clear();
		s.palanques.clear();
		s.portes.clear();
		s.barreres.clear();
		s.caixes.clear();
		s.lamps.clear();

		s.electricitats.clear();
		s.turretas.clear();
		
		s.bullets.clear();
		
		//Try to collect Garbage
		System.gc();
		
		for (int i = 0; i < 5; i++) {
			s.bullets.addActor(new Bullet(0, 0, s));
			
		}
		
		
		
		for (int i = 0; i < Level.map.height; i++) {
			for (int j = 0; j < Level.map.width; j++) {
				switch (Level.map.layers.get(Level.objects[0]).tiles[i][j]) {
				case 15:

					s.cargols.addActor( new Cargol((j*64),((Level.map.height-i-1)*64),false, s));

					break;

				case 30:

					s.cargols.addActor( new Cargol((j*64),((Level.map.height-i-1)*64),true, s));

					break;

				case 60:
					s.palanques.addActor( new Palanca((j*64),((Level.map.height-i-1)*64),0,false,s));


					break;
				case 75:
					s.palanques.addActor( new Palanca((j*64),((Level.map.height-i-1)*64),1,false,s));


					break;
				case 90:
					s.palanques.addActor( new Palanca((j*64),((Level.map.height-i-1)*64),2,false,s));


					break;
				case 105:
					s.palanques.addActor( new Palanca((j*64),((Level.map.height-i-1)*64),3,false,s));
					break;
					
					
				case 58:
					s.palanques.addActor( new Palanca((j*64),((Level.map.height-i-1)*64),0,true,s));


					break;
				case 73:
					s.palanques.addActor( new Palanca((j*64),((Level.map.height-i-1)*64),1,true,s));


					break;
				case 88:
					s.palanques.addActor( new Palanca((j*64),((Level.map.height-i-1)*64),2,true,s));


					break;
				case 103:
					s.palanques.addActor( new Palanca((j*64),((Level.map.height-i-1)*64),3,true,s));
					break;

					


				case 120:

					s.portes.addActor(new Porta((j*64),((Level.map.height-i-1)*64),Tiles.tile2num(Level.map.layers.get(Level.objects[0]).tiles[i-1][j])+Tiles.tile2num(Level.map.layers.get(Level.objects[0]).tiles[i-2][j])*10, s));

					break;
				case 135:

					s.barreres.addActor( new Barrera((j*64),((Level.map.height-i-1)*64),Tiles.tile2num(Level.map.layers.get(Level.objects[0]).tiles[i-1][j])+Tiles.tile2num(Level.map.layers.get(Level.objects[0]).tiles[i-2][j])*10,s));

					break;
					
				case 150:

					s.coals.addActor(new Coal((j*64),((Level.map.height-i-1)*64),s));
					break;
					
				case 165:

					s.caixes.addActor(new Caixa((j*64),((Level.map.height-i-1)*64),false,s));
					break;
					
				case 164:

					s.caixes.addActor(new Caixa((j*64),((Level.map.height-i-1)*64),true,s));
					break;
					
				case 180:

					s.lamps.addActor(new Lamp((j*64),((Level.map.height-i-1)*64),i,j,s));
					break;
					
				case 195:

					s.electricitats.addActor(new Electricitat((j*64),((Level.map.height-i-1)*64),0,s));
					break;
				case 210:

					s.electricitats.addActor(new Electricitat((j*64),((Level.map.height-i-1)*64),90,s));
					break;
					
				case 194:

					s.electricitats.addActor(new Electricitat((j*64),((Level.map.height-i-1)*64),270,s));
					break;
				case 209:

					s.electricitats.addActor(new Electricitat((j*64),((Level.map.height-i-1)*64),180,s));
					break;
					
					
				case 225:

					s.turretas.addActor(new Turreta((j*64),((Level.map.height-i-1)*64),s));
					break;
					
				}
				
				
			}
		}
		
		//Try to collect Garbage
		System.gc();
	}
	
	
}


