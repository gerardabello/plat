package com.gerardas.platDesktop;


import aurelienribon.tweenengine.Tween;

import com.badlogic.gdx.Game;
import com.gerardas.platDesktop.Entities.Player;
import com.gerardas.platDesktop.Entities.TweenAccessors.PlayerAccessor;




public class Plat extends Game{

	public static  MainMenuScreen mainMenuScr;
	public  static GameScreen gameScr;
	public static boolean doneSplash;
	public static SplashScreen Splash;


	@Override
	public void create() {
		
		Tween.registerAccessor(Player.class, new PlayerAccessor());
		
		

		Splash=new SplashScreen(this);
		this.setScreen(Splash);
		

	}

//	@Override
//	public void resume() {
//		if (doneSplash){
//			gameScr.resume();
//		}else{
//			Splash.resume();
//		}
//		
//	}
//	
//	
//
//	@Override
//	public void pause() {
//		// TODO Auto-generated method stub
//		if (doneSplash){
//			gameScr.pause();
//			doneSplash=false;
//		}else{
//			Splash.pause();
//		}
//	}

}