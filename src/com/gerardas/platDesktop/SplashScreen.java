package com.gerardas.platDesktop;


import com.badlogic.gdx.Game;
import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.Screen;
import com.badlogic.gdx.graphics.GL20;
import com.badlogic.gdx.graphics.Texture;
import com.badlogic.gdx.graphics.g2d.SpriteBatch;

public class SplashScreen implements Screen {

	private SpriteBatch spriteBatch;
	private Texture grass;
	private Game myGame;
	private int count=0;

	/**
	 * Constructor for the splash screen
	 * @param g Game which called this splash screen.
	 */
	public SplashScreen(Game g)
	{
		myGame = g;
		spriteBatch = new SpriteBatch();
		grass = new Texture(Gdx.files.internal("data/img/grass.png"));

	}

	@Override
	public void render(float delta){

		Gdx.gl.glClear(GL20.GL_COLOR_BUFFER_BIT);
		spriteBatch.begin();

		spriteBatch.disableBlending();

		for(int i = 0; i <= (Gdx.graphics.getWidth()/grass.getWidth()); i++) {
			for(int j = 0; j <= (Gdx.graphics.getHeight()/grass.getHeight()); j++) {
				spriteBatch.draw(grass,grass.getWidth()*i ,grass.getHeight()*j ); 
			}
		}

		spriteBatch.end();
		

		Textures.load_Textures();
		if(count==30){

			count=0;
			Plat.doneSplash=true;
			if(Plat.gameScr==null){
				Gdx.app.log("NEW", "GS");
				Plat.mainMenuScr= new MainMenuScreen(myGame);
				Plat.gameScr= new GameScreen(myGame);
			}else{
				Plat.gameScr.resume();
				myGame.setScreen(Plat.gameScr);
			}

		}else{
			count++;
		}

	}

	@Override
	public void show(){

	}

	@Override
	public void dispose() {
		// TODO Auto-generated method stub

	}

	@Override
	public void hide() {
		// TODO Auto-generated method stub

	}

	@Override
	public void pause() {
		// TODO Auto-generated method stub

	}

	@Override
	public void resize(int arg0, int arg1) {
		// TODO Auto-generated method stub

	}

	@Override
	public void resume() {
		// TODO Auto-generated method stub

	}


}
