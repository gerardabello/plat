package com.gerardas.platDesktop.UI;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.graphics.Texture;
import com.badlogic.gdx.graphics.g2d.BitmapFont;
import com.badlogic.gdx.graphics.g2d.TextureRegion;
import com.badlogic.gdx.graphics.g2d.freetype.FreeType;
import com.badlogic.gdx.graphics.g2d.freetype.FreeTypeFontGenerator;
import com.badlogic.gdx.scenes.scene2d.Stage;
import com.gerardas.platDesktop.GameScreen;

public class UI {


	public Stage stage;
	private GameScreen screen;
	private Texture tex;
	private TextureRegion texRControl, texLControl, texJControl;
	private ControlButton LButton, RButton, JButton;



	public BitmapFont font;
	public static final String FONT_CHARACTERS = "abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ0123456789][_!$%#@?-+=()*&.;,{}<>";
	       


	public UI(final GameScreen screen) {
		this.screen = screen;
		
		stage=new Stage(Gdx.graphics.getWidth(), Gdx.graphics.getHeight(), true);
		
		
		
		tex = new Texture(Gdx.files.internal("data/img/ui.png"));

		
		
		createControls();
		createLabels();
		
		FreeTypeFontGenerator fg = new FreeTypeFontGenerator(Gdx.files.internal("data/font/repulsor.ttf"));
		font = fg.generateFont(52, FONT_CHARACTERS, false);
		font.setColor(0, 0, 0, .5f);


		
	}

	


	private void createLabels() {
//		labelC=new CLabel()
		
	}




	private void createControls() {
		float s = 150;

		
		texJControl=new TextureRegion(tex, 128*2, 0, 128, 128);
		texRControl=new TextureRegion(tex, 128*1, 0, 128, 128);
		texLControl=new TextureRegion(tex, 0, 0, 128, 128);
		
		LButton= new ControlButton(texLControl,0,0,s);
		RButton= new ControlButton(texRControl,s,0,s);
		JButton= new ControlButton(texJControl,Gdx.graphics.getWidth()-s,0,s);

		stage.addActor(LButton);
		stage.addActor(RButton);
		stage.addActor(JButton);
		
	}




	public void dispose() {
		stage.dispose();
	}



	public void draw() {
		stage.draw();
	}
	
	
	public boolean move_left(){
		return LButton.isTouching();
	}

	
	public boolean move_right(){
		return RButton.isTouching();
	}
	
	
	public boolean move_jump(){
		return JButton.isTouching();
	}
	

}
