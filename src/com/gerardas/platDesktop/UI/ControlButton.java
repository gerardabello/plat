package com.gerardas.platDesktop.UI;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.graphics.g2d.SpriteBatch;
import com.badlogic.gdx.graphics.g2d.TextureRegion;
import com.badlogic.gdx.scenes.scene2d.Actor;
import com.badlogic.gdx.scenes.scene2d.InputEvent;
import com.badlogic.gdx.scenes.scene2d.utils.ClickListener;

public class ControlButton extends Actor {

	private boolean touching;
	private TextureRegion texr;


	public ControlButton(TextureRegion tex, float x, float y, float s) {
		this.setPosition(x, y);
		this.setSize(s, s);
		texr= tex;

		setEvents();

	}


	private void setEvents() {
		this.addListener(new ClickListener() {
			public boolean touchDown(InputEvent event, float x, float y, int pointer, int button)  {
				touching = true;

				return true;
		
			}
			
			public void touchUp(InputEvent event, float x, float y, int pointer, int button)  {
				touching = false;

		
			}
			
		});	

	}





	public void touchUp(float x,float y,int pointer){
		touching =false;

	}



	public boolean isTouching(){
		return touching;
	}


	public void draw(SpriteBatch batch,float parentAlpha){

		batch.draw(texr,
				getX(),
				getY(),
				getOriginX(),
				getOriginY(),
				getWidth(),
				getHeight(),
				getScaleX(),
				getScaleY(),
				getRotation());


	}

}
