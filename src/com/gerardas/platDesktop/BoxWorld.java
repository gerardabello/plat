package com.gerardas.platDesktop;

import java.util.ArrayList;
import java.util.List;

import javax.xml.bind.annotation.XmlElementDecl.GLOBAL;

import box2dLight.ConeLight;
import box2dLight.PointLight;
import box2dLight.RayHandler;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.Input.Keys;
import com.badlogic.gdx.graphics.Camera;
import com.badlogic.gdx.graphics.Color;
import com.badlogic.gdx.math.Vector2;
import com.badlogic.gdx.physics.box2d.Body;
import com.badlogic.gdx.physics.box2d.BodyDef;
import com.badlogic.gdx.physics.box2d.BodyDef.BodyType;
import com.badlogic.gdx.physics.box2d.Box2DDebugRenderer;
import com.badlogic.gdx.physics.box2d.CircleShape;
import com.badlogic.gdx.physics.box2d.Contact;
import com.badlogic.gdx.physics.box2d.ContactImpulse;
import com.badlogic.gdx.physics.box2d.ContactListener;
import com.badlogic.gdx.physics.box2d.Fixture;
import com.badlogic.gdx.physics.box2d.FixtureDef;
import com.badlogic.gdx.physics.box2d.Manifold;
import com.badlogic.gdx.physics.box2d.PolygonShape;
import com.badlogic.gdx.physics.box2d.World;
import com.badlogic.gdx.physics.box2d.WorldManifold;
import com.badlogic.gdx.physics.box2d.joints.RevoluteJointDef;
import com.badlogic.gdx.scenes.scene2d.Stage;
import com.badlogic.gdx.utils.TimeUtils;

public class BoxWorld  {  

	GameScreen screen;

	public static final int scale = 3;

	public World world;  
	Vector2 gravity,normalUp;
	final static float MAX_VELOCITY = 5*scale;
	final static float JUMP_I = 47*scale;

	static final int BOX_VELOCITY_ITERATIONS=6;  
	static final int BOX_POSITION_ITERATIONS=2;
	Box2DDebugRenderer debugRenderer;
	private Fixture playerPhysicsFixture;
	private Fixture playerSensorFixture;

	public Body player;
	private float stillTime;
	private long lastGroundTime;

	private boolean isGrounded;

	public RayHandler rayHandler;

	public Body[][] bodyMap;

	private boolean keyW;

	private boolean jump_allowed,jump,left,right;

	public boolean enabled=true;  

	List<Body> perDestruir = new ArrayList<Body>();




	public BoxWorld(GameScreen s) {

		screen = s;
		debugRenderer = new Box2DDebugRenderer();  
		gravity=new Vector2(0,-200);
		normalUp=new Vector2(0,1);




	}




	public Body createBullet() {
		BodyDef bodyDef = new BodyDef(); 
		bodyDef.type = BodyType.DynamicBody;
		bodyDef.bullet=true;
		bodyDef.fixedRotation=true;
		bodyDef.gravityScale=0;
		bodyDef.active=false;
		Body body = world.createBody(bodyDef);
		body.setUserData(5);

		PolygonShape bodyShape = new PolygonShape();

		float w=1f/15*scale;
		float h=1f/4*scale;
		bodyShape.setAsBox(w,h);

		FixtureDef fixtureDef=new FixtureDef();
		fixtureDef.density=.5f;
		fixtureDef.shape=bodyShape;

		body.createFixture(fixtureDef);
		bodyShape.dispose();

		return body;

	}



	private Body createPlayer () {
		BodyDef def = new BodyDef();
		def.type = BodyType.DynamicBody;
		def.position.set(scale*7, scale*7);
		def.bullet=false;
		def.allowSleep=false;
		Body box = world.createBody(def);
		box.setUserData(1);

		CircleShape top = new CircleShape();
		top.setRadius(0.3f*scale);
		top.setPosition(new Vector2(0, -0.03f*scale));
		playerPhysicsFixture = box.createFixture(top, 1);
		playerPhysicsFixture.setFriction(0);
		top.dispose();

		CircleShape circle = new CircleShape();
		circle.setRadius(0.27f*scale);
		circle.setPosition(new Vector2(0, -.23f*scale));
		playerSensorFixture = box.createFixture(circle, 0);
		circle.dispose();


		box.setBullet(true);


		return box;
	}


	public void setTiles(int[][] tiles){


		setWorld();


		bodyMap = new Body[Level.map.height][Level.map.width];


		for (int i = 0; i < Level.map.height; i++) {
			for (int j = 0; j < Level.map.width; j++) {
				if(Tiles.col(tiles[i][j])) {
					bodyMap[i][j]=createNewTile((j+0.5f)*scale,((Level.map.height-i-1)+0.5f)*scale);
				}
			}
		}

		System.gc();

	}

	private void setWorld() {
		if(world!=null){
			rayHandler.dispose();
			world.dispose();
		}
		enabled=true;
		world=new World(new Vector2(0, -200), true);

		world.step(0.001f, BOX_VELOCITY_ITERATIONS, BOX_POSITION_ITERATIONS);

		world.setContactListener(new ContactListener() {

			@Override
			public void preSolve(Contact contact, Manifold oldManifold) {
				
				if((contact.getFixtureA() == playerSensorFixture || contact.getFixtureB() == playerSensorFixture)){
					isGrounded=true;
				}

				if(contact.isEnabled() && contact.isTouching()){
					if(contact.getFixtureA().getBody().getUserData().equals(5)){
						perDestruir.add(contact.getFixtureA().getBody());
						if(contact.getFixtureB().getBody()==player){
							screen.stage.mort();
						}
					}else if(contact.getFixtureB().getBody().getUserData().equals(5)){
						perDestruir.add(contact.getFixtureB().getBody());
						if(contact.getFixtureA().getBody()==player){
							screen.stage.mort();
						}
					}
				}


			}

			@Override
			public void beginContact(Contact contact) {
				

			}

			@Override
			public void endContact(Contact contact) {
				// TODO Auto-generated method stub

			}

			@Override
			public void postSolve(Contact contact, ContactImpulse impulse) {
				// TODO Auto-generated method stub

			}

		});


		rayHandler = new RayHandler(world);

		rayHandler.setBlur(true);
		rayHandler.setBlurNum(2);
		rayHandler.setCulling(true);//TODO el culling no funciona si es gira la camera

		player = createPlayer();
		player.setActive(true);
		player.setAwake(true);
		player.setFixedRotation(true);



		rayHandler.setAmbientLight(1, 1, 1, 1);


		System.gc();



	}


	public Body createNewTile(float x, float y) {
		BodyDef bodyDef = new BodyDef(); 
		bodyDef.type = BodyType.StaticBody;
		bodyDef.position.set(x,y);
		bodyDef.angle=0;
		bodyDef.bullet=false;
		Body body = world.createBody(bodyDef);
		body.setUserData(0);

		PolygonShape bodyShape = new PolygonShape();

		float w=1f/2*scale;
		float h=1f/2*scale;
		bodyShape.setAsBox(w,h);

		FixtureDef fixtureDef=new FixtureDef();
		fixtureDef.density=1;
		fixtureDef.shape=bodyShape;

		body.createFixture(fixtureDef);
		bodyShape.dispose();

		return body;
	}


	public Body createNewDynBox(float x, float y,boolean heavy) {
		BodyDef bodyDef = new BodyDef(); 
		bodyDef.type = BodyType.DynamicBody;
		bodyDef.position.set((x+32)/64*scale,(y+32)/64*scale);
		bodyDef.angle=0;
		bodyDef.bullet=false;
		Body body = world.createBody(bodyDef);
		body.setUserData(2);


		PolygonShape bodyShape = new PolygonShape();

		float w=1f/2*scale;
		float h=1f/2*scale;
		bodyShape.setAsBox(w,h);

		FixtureDef fixtureDef=new FixtureDef();
		fixtureDef.density=(heavy? 30f:1.1f);
		fixtureDef.shape=bodyShape;

		body.createFixture(fixtureDef);
		bodyShape.dispose();

		return body;

	}


	public Body[] createChain(Body pb){
		Body[] bodies= new Body[4];
		PolygonShape shape = new PolygonShape();
		shape.setAsBox(0.05f*scale, 0.16f*scale);

		FixtureDef fd = new FixtureDef();
		fd.shape = shape;
		fd.density = 2.0f;
		fd.friction = 0.2f;

		RevoluteJointDef jd = new RevoluteJointDef();
		jd.collideConnected = false;


		Body prevBody = pb;

		Vector2 anchor = new Vector2(prevBody.getPosition().x, prevBody.getPosition().y-0.50f*scale);

		for (int i = 0; i < bodies.length; i++) {
			BodyDef bd = new BodyDef();
			bd.type = BodyType.DynamicBody;
			bd.angularDamping=2f;
			bd.bullet=false;
			bd.linearDamping=0.2f;
			if(i==0){
				bd.position.set(prevBody.getPosition().x, prevBody.getPosition().y-0.6f*scale);
			}else{
				bd.position.set(prevBody.getPosition().x, prevBody.getPosition().y-0.4f*scale);
			}
			Body body = world.createBody(bd);
			body.setUserData(3);

			if(i== bodies.length-1){
				fd.density=0.2f;
				jd.enableLimit=true;
				shape.setAsBox(0.2f*scale, 0.2f*scale);
			}
			body.createFixture(fd);

			jd.initialize(prevBody, body, anchor);
			jd.collideConnected=false;
			world.createJoint(jd);


			prevBody = body;

			anchor.set(prevBody.getPosition().x, prevBody.getPosition().y-0.2f*scale);

			bodies[i]=body;
		}

		shape.dispose();

		return bodies;
	}






	public void setAngle(float angle){
		player.setAngularVelocity((-angle-(player.getAngle()/2/3.14f*360))/2);
		gravity.set(0,-200);
		gravity.rotate(-angle);
		normalUp.set(0, 1);
		normalUp.rotate(-angle);
		world.setGravity(gravity);
	}

	public void debugRender(Camera cam){
		debugRenderer.render(world, cam.combined);  
	}


	public void renderLights(Camera cam){
		rayHandler.setCombinedMatrix(cam.combined);
		rayHandler.updateAndRender();
	}

	public void step(float dt) {
		if(enabled){
			playerInteractions();
			isGrounded=false;
			world.step(dt, BOX_VELOCITY_ITERATIONS, BOX_POSITION_ITERATIONS);  		


			for (int i = 0; i < perDestruir.size(); i++) {
				perDestruir.get(i).setActive(false);
				perDestruir.remove(i);
			}

		}	
	}


	private void playerInteractions() {
		Vector2 vel = player.getLinearVelocity();
		Vector2 pos = player.getPosition();

		left = Gdx.input.isKeyPressed(Keys.A) || screen.ui.move_left();
		right=Gdx.input.isKeyPressed(Keys.D) || screen.ui.move_right();
		jump=Gdx.input.isKeyPressed(Keys.W) || screen.ui.move_jump();


		boolean grounded = isGrounded;
		if (grounded) {
			lastGroundTime = TimeUtils.nanoTime();
		} else {
			if (TimeUtils.nanoTime() - lastGroundTime < 100000000) {
				grounded = true;
			}
		}

		if(gravity.y<-1 ||gravity.y>1 ){
			// cap max velocity on x
			if (Math.abs(vel.x) > MAX_VELOCITY) {
				vel.x = Math.signum(vel.x) * MAX_VELOCITY;
				player.setLinearVelocity(vel.x, vel.y);
			}
		}else if(gravity.x<-1 ||gravity.x>1){
			// cap max velocity on y
			if (Math.abs(vel.y) > MAX_VELOCITY) {
				vel.y = Math.signum(vel.y) * MAX_VELOCITY;
				player.setLinearVelocity(vel.x, vel.y);
			}

		}else{
			System.exit(1);
		}



		if(gravity.y<-1 ||gravity.y>1 ){
			// calculate stilltime & damp
			if (!left && !right) {
				stillTime += Gdx.graphics.getDeltaTime();
				player.setLinearVelocity(vel.x * 0.9f, vel.y);
			} else {
				stillTime = 0;
			}
		}else if(gravity.x<-1 ||gravity.x>1){
			// calculate stilltime & damp
			if (!left && !right) {
				stillTime += Gdx.graphics.getDeltaTime();
				player.setLinearVelocity(vel.x , vel.y* 0.9f);
			} else {
				stillTime = 0;
			}

		}else{
			System.exit(1);
		}


		// disable friction while jumping
		if (!grounded) {
			playerPhysicsFixture.setFriction(0f);
			playerSensorFixture.setFriction(0f);
		} else {
			if (!left && !right && stillTime > 0.2) {
				playerPhysicsFixture.setFriction(100f);
				playerSensorFixture.setFriction(100f);
			} else {
				playerPhysicsFixture.setFriction(0.2f);
				playerSensorFixture.setFriction(0.2f);
			}

		}

		// since Box2D 2.2 we need to reset the friction of any existing contacts
		List<Contact> contacts = world.getContactList();
		for (int i = 0; i < world.getContactCount(); i++) {
			Contact contact = contacts.get(i);
			contact.resetFriction();
		}


		normalUp.rotate(90);
		// apply left impulse, but only if max velocity is not reached yet
		if (left && vel.x > -MAX_VELOCITY) {
			player.applyLinearImpulse(3f*scale*normalUp.x, 3f*scale*normalUp.y, pos.x, pos.y);
		}

		normalUp.rotate(180);
		// apply right impulse, but only if max velocity is not reached yet
		if (right && vel.x < MAX_VELOCITY) {
			player.applyLinearImpulse(3f*scale*normalUp.x, 3f*scale*normalUp.y, pos.x, pos.y);
		}
		normalUp.rotate(90);





		if(!jump)jump_allowed=true;



		// jump, but only when grounded
		if (jump && jump_allowed) {
			System.out.println("jump ");
			jump = false;
			if (grounded) {
				System.out.println("jump OK");


				jump_allowed=false;
				if(gravity.y<-1 ||gravity.y>1 ){


					player.setLinearVelocity(vel.x, 0);
					System.out.println("jump before: " + player.getLinearVelocity());
					player.applyLinearImpulse(normalUp.x*JUMP_I, normalUp.y*JUMP_I, pos.x, pos.y);
					System.out.println("jump, " + player.getLinearVelocity());
				}else if(gravity.x<-1 ||gravity.x>1){


					player.setLinearVelocity(0, vel.y);
					System.out.println("jump before: " + player.getLinearVelocity());
					player.applyLinearImpulse(normalUp.x*JUMP_I, normalUp.y*JUMP_I, pos.x, pos.y);
					System.out.println("jump, " + player.getLinearVelocity());

				}else{
					System.exit(1);
				}



			}
		}

		player.setActive(true);


	}  

	


	public void dispose(){
		rayHandler.dispose();
		world.dispose();
	}

}  