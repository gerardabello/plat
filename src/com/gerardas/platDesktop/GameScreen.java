package com.gerardas.platDesktop;

import com.badlogic.gdx.Game;
import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.Input.Keys;
import com.badlogic.gdx.InputMultiplexer;
import com.badlogic.gdx.Screen;
import com.badlogic.gdx.graphics.Camera;
import com.badlogic.gdx.graphics.GL10;
import com.badlogic.gdx.graphics.OrthographicCamera;
import com.badlogic.gdx.graphics.Texture;
import com.badlogic.gdx.graphics.g2d.SpriteBatch;
import com.badlogic.gdx.graphics.g2d.tiled.SimpleTileAtlas;
import com.badlogic.gdx.graphics.g2d.tiled.TileMapRenderer;
import com.badlogic.gdx.input.GestureDetector;
import com.badlogic.gdx.input.GestureDetector.GestureListener;
import com.badlogic.gdx.math.Vector2;
import com.badlogic.gdx.math.Vector3;
import com.badlogic.gdx.scenes.scene2d.Actor;
import com.gerardas.platDesktop.Entities.Porta;
import com.gerardas.platDesktop.UI.UI;



public class GameScreen implements Screen{
	Game game;

	public GameStage stage;
	public BoxWorld boxworld;

	SpriteBatch batch;


	public UI ui;


	public Camera cam;


	private float sdDt;

	private final int ini_level=1;  //0 a no ser que es vulgui testejar un nivell

	private Vector3 desiredCamPos;

	private GestureDetector gestureDetector;

	private InputMultiplexer ip;

	private CameraController camControl;






	







	public GameScreen(Game game) {
		this.game = game;
		Level.screen=this;
		
		Objects.ini();
		
		
		
		
		boxworld=new BoxWorld(this);

		ui = new UI(this);







		stage=new GameStage(Gdx.graphics.getWidth(), Gdx.graphics.getHeight(), false, this);
		Gdx.input.setInputProcessor(stage);


		batch=stage.getSpriteBatch();
		cam=stage.getCamera();
		desiredCamPos=new Vector3();



		Level.ini_level();

		Tiles.ini();

		Level.load(ini_level);
		
		
		
		
		camControl=new CameraController();
		gestureDetector = new GestureDetector(20, 0.5f, 2, 0.15f, camControl);

		
		
		ip = new InputMultiplexer();

		ip.addProcessor(ui.stage);
		ip.addProcessor(gestureDetector);
		ip.addProcessor(stage);


		Gdx.input.setInputProcessor(ip);



	}


	@Override
	public void render(float deltaTime) {
//		cam.viewportHeight=Gdx.graphics.getHeight()*2;
//		cam.viewportWidth=Gdx.graphics.getWidth()*2;
		cam.update();


		sdDt=Math.min(deltaTime, .04f);

		boxworld.setAngle(stage.cameraAngle);


		if(!stage.player.isAnimating())boxworld.step(sdDt);


		
		
		updateCam();















		stage.act(sdDt);
		stage.setCamera(cam);


		Gdx.gl.glClear(GL10.GL_COLOR_BUFFER_BIT);

		Level.gir_upd(deltaTime);




		// this is the main render function
		renderer();


		//Gdx.app.log("Binds",	 Integer.toString(batch.renderCalls));


	}


	private void updateCam() {
		
		desiredCamPos.set(stage.player.getCenterX(), stage.player.getCenterY(), 0);
		if(Utils.distSquared(cam.position.x, cam.position.y, desiredCamPos.x, desiredCamPos.y)>30){
			
			if(!stage.player.isAnimating())cam.position.lerp(desiredCamPos, .1f);
		}
		
		
	

		cam.update();
	}


	private void renderer() {

		Gdx.gl.glClear(GL10.GL_COLOR_BUFFER_BIT);






		Level.mapR.getProjectionMatrix().set(cam.combined);
		cam.update();
		Level.mapR.render((OrthographicCamera) cam,Level.back);


		//Tiles i shadows
		if(Level.DrawShadows){
			cam.translate((float) (-(Level.dxS*2)*Math.sin(((stage.cameraAngle+225)/360)*2*Math.PI)), (float) (-(Level.dxS*2)*Math.cos(((stage.cameraAngle+225)/360)*2*Math.PI)), 0);
			cam.update();
			Level.mapR.render((OrthographicCamera) cam,Level.sombra);






			cam.translate((float) ((Level.dxS*2)*Math.sin(((stage.cameraAngle+225)/360)*2*Math.PI)), (float) ((Level.dxS*2)*Math.cos(((stage.cameraAngle+225)/360)*2*Math.PI)), 0);
			cam.update();
		}

		stage.draw();




		Level.mapR.render((OrthographicCamera) cam,Level.layers);

		Level.mapR.render((OrthographicCamera) cam,Level.pipes);
		
		stage.getSpriteBatch().begin();
		for (Actor a : stage.portes.getChildren()) {
			((Porta)a).drawCartell(stage.getSpriteBatch(), 1);
			
		}
		stage.getSpriteBatch().end();




		cam.viewportWidth=cam.viewportWidth/(64.0f/BoxWorld.scale);		
		cam.viewportHeight=cam.viewportHeight/(64.0f/BoxWorld.scale);	
		cam.position.x=cam.position.x/(64.0f/BoxWorld.scale);	
		cam.position.y=cam.position.y/(64.0f/BoxWorld.scale);	
		cam.update();

		boxworld.renderLights(cam);

		if(Gdx.input.isKeyPressed(Keys.X))boxworld.debugRender(cam);

		cam.viewportWidth=cam.viewportWidth*(64.0f/BoxWorld.scale);		
		cam.viewportHeight=cam.viewportHeight*(64.0f/BoxWorld.scale);		
		cam.position.x=cam.position.x*(64.0f/BoxWorld.scale);	
		cam.position.y=cam.position.y*(64.0f/BoxWorld.scale);
		cam.update();
		
		



		ui.draw();

	}


	@Override
	public void resume() {
		batch=new SpriteBatch();

		Objects.tex = new Texture(Gdx.files.internal("data/img/obj2.png"));

		Level.atlas = new SimpleTileAtlas(Level.map, Gdx.files.internal("data/world/"));
		Level.mapR= new TileMapRenderer(Level.map,
				Level.atlas,
				100,
				100);
	}

	@Override
	public void pause() {
		batch.dispose();

		Level.mapR.dispose();
		Level.atlas.dispose();

		Objects.tex.dispose();

		Gdx.app.log("GS", "Paused");

	}

	@Override
	public void dispose() {


	}


	@Override
	public void hide() {
		// TODO Auto-generated method stub

	}


	@Override
	public void resize(int width, int height) {


	}


	@Override
	public void show() {
		// TODO Auto-generated method stub

	}
	
	
	
	
	class CameraController implements GestureListener {
		float velX, velY;
		boolean flinging = false;
		float initialScale = 1;
		

		public boolean touchDown (float x, float y, int pointer, int button) {
			flinging = false;
			initialScale = ((OrthographicCamera)cam).zoom;
			return false;
		}


		@Override
		public boolean longPress (float x, float y) {
			Gdx.app.log("GestureDetectorTest", "long press at " + x + ", " + y);
			return false;
		}

		@Override
		public boolean fling (float velocityX, float velocityY, int button) {
			flinging = true;
			velX = ((OrthographicCamera)cam).zoom * velocityX * 0.5f;
			velY = ((OrthographicCamera)cam).zoom * velocityY * 0.5f;
			return false;
		}

		@Override
		public boolean pan (float x, float y, float deltaX, float deltaY) {
			cam.position.add(-deltaX * ((OrthographicCamera)cam).zoom, deltaY * ((OrthographicCamera)cam).zoom, 0);
			return false;
		}

		@Override
		public boolean zoom (float originalDistance, float currentDistance) {
			float ratio = originalDistance / currentDistance;
			((OrthographicCamera)cam).zoom = initialScale * ratio;
			System.out.println(((OrthographicCamera)cam).zoom);
			return false;
		}

		@Override
		public boolean pinch (Vector2 initialFirstPointer, Vector2 initialSecondPointer, Vector2 firstPointer, Vector2 secondPointer) {
			return false;
		}

		public void update () {
			if (flinging) {
				velX *= 0.98f;
				velY *= 0.98f;
				cam.position.add(-velX * Gdx.graphics.getDeltaTime(), velY * Gdx.graphics.getDeltaTime(), 0);
				if (Math.abs(velX) < 0.01f) velX = 0;
				if (Math.abs(velY) < 0.01f) velY = 0;
			}
		}


		@Override
		public boolean tap(float arg0, float arg1, int arg2, int arg3) {
			// TODO Auto-generated method stub
			return false;
		}
	}




}
